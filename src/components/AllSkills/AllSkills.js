import React, { useState, useEffect } from 'react'
import SkillItem from '../SkillItems/SkillItem';

const AllSkills = () => {

  // USE STATE
  const [skills, setSkills] = useState([]);
  const [userSkills, setUserSkills] = useState([]);
  const [userName, setUserName] = useState([]);


  const addSkillHandler = skillToAdd => {
    if(userSkills.find(el => el.id === skillToAdd.id) === undefined){
          setUserSkills([...userSkills, skillToAdd])
    }
    else{
      alert("You are already mastered this skill young grass hopper")
    }
  }
  
  const deleteSkillHandler = (skillToDelete) => {
    setUserSkills(userSkills.filter(el => el.id !== skillToDelete.id));
};

  // USE EFFECT
  useEffect(() => {
    getSkills();
    getName();
  }, [])

  // API
  const getSkills = async () => {
    const response = await fetch(
      "https://programmer-skills-api.herokuapp.com/skills"
    );
    const data = await response.json();
    setSkills(data);
  }
  const getName = () => {
    setUserName(JSON.parse(localStorage.getItem('username')))
  }

  return (
    <main className="skills-page-container">
      <div>
        <h1>All Skills</h1>
        <ul className="skills-container">
          {skills.map(skill => (
            <SkillItem
              skill={skill}
              addSkill={addSkillHandler}
            />
          ))}
        </ul>
      </div>
      <div>
        <h1>{userName} Skills</h1>
        <ul className="user-skills-container">
          {userSkills.map(userskills => (
            <SkillItem
              skill={userskills}
              key={userskills.id}
              deleteSkill={deleteSkillHandler}
              obtainedSkill={true}
            />
          ))}
        </ul>
      </div>

    </main>
  )
}

export default AllSkills