import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {
    return(
        <main>
            <h1>Sorry, the page you were looking for does not exist...</h1>
            <Link to="/">Take me back Home</Link>
        </main>
    )
}

export default NotFound