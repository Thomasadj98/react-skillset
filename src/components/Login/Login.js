import React, { useEffect, useState } from "react";
import { Redirect } from "react-router-dom";

const Login = () => {

  // USE STATE
  const [inputText, setInputText] = useState("");
  const [name, setName] = useState("");
  const [registeredUser, setRegisteredUser] = useState(false);

  //USE EFFECT
  useEffect(() => {
    getLocalUsername();
  }, []);

  useEffect(() => {
    saveToLocalStorage();
  }, [name]);

  // FUNCTION
  const nameHandler = (event) => {
    setInputText(event.target.value);
  };

  const submitNameHandler = (event) => {
    if (inputText === "") {
      alert("Please enter a username")
    }
    else {
      event.preventDefault();
      setName(inputText);
      setInputText("");
      setRegisteredUser(true);
    }

  };

  const saveToLocalStorage = () => {
    console.log(name);
    if (name.trim() === "") {
      return;
    }
    localStorage.setItem("username", JSON.stringify(name));
  };

  const getLocalUsername = () => {
    const existingUser = localStorage.getItem("username");
    if (!existingUser) {
      // Sad! Do nothing (registeredUser is already false)
      return;
    }
    let localUsername = JSON.parse(localStorage.getItem("username"));
    setName(localUsername);
    setRegisteredUser(true);
  };

  return (
    <main className="login-container">
      {!registeredUser ? (
        <div>
          <h1>Welcome {name}</h1>
          <form className="form-container" onSubmit={submitNameHandler}>
            <input
              className="form-input"
              value={inputText}
              onChange={nameHandler}
              type="text"
              placeholder="Name..."
            />
            <button className="form-btn" type="submit">
              Register
            </button>
          </form>
        </div>
      ) : (
        <Redirect to="/allskills" />
      )}
    </main>
  );
};

export default Login;
