import React, { useState } from 'react'

const SkillItem = ({ skill, addSkill, deleteSkill, obtainedSkill }) => {



    return (
        <main>
            <li className="skill-card">
                <h2>{skill.name}</h2>
                <p>{skill.description}</p>
                {!obtainedSkill ?
                    (
                        <button className="skill-item-btn" onClick={() => addSkill(skill)}>Add skill</button>
                    ) :
                    (
                        <button className="skill-item-btn" onClick={() => deleteSkill(skill)}>Delete skill</button>
                    )
                }
            </li>
        </main>
    )
}

export default SkillItem