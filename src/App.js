import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import NotFound from './components/NotFound/NotFound'
import Login from './components/Login/Login'
import React, { useState, useEffect } from 'react'
import AllSkills from './components/AllSkills/AllSkills';

function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <h1>Welcome to skillset app</h1>

        <Switch>
          <Route path="/" exact component={ Login } />
          <Route path="/allskills" component={ AllSkills } />
          <Route path="*" component={ NotFound } />
        </Switch>

      </div>
    </BrowserRouter>
  );
}

export default App;
